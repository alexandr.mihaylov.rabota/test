# -*- coding: utf-8 -*-
#!/usr/bin/python
import json, telebot,os,os.path,configparser
from websocket import create_connection
api_token = "1872161698:AAHP9a05Fp3cCKJ2WWFj45C5UnthsyquONI"
api_url = "https://api.telegram.org/bot" + api_token + "/sendMessage"
bot_id = 381192306
bot = telebot.TeleBot(api_token, parse_mode=None) 
config = configparser.ConfigParser()
try:
    ws = create_connection("wss://srv2.warforgalaxy.com:9982/ws", sslopt={"check_hostname": False})
    request = "{\"method\":\"api.statistics.online\",\"params\":{},\"id\":1}"
    ws.send(request)
    result =  ws.recv()
    ws.close()
    res = json.loads(result)
    # количество игроков онлайн
    print (res["result"])
    if os.path.isfile('/tmp/flag_server_warning.ini') == True:
        path = os.path.join(os.path.abspath(os.path.dirname(__file__)), '/tmp/flag_server_warning.ini')
        os.remove(path)
        bot.send_message(bot_id, text='\U0001F7E2 server reachable')
except:
    if os.path.isfile('/tmp/flag_server_warning.ini') == False:
        bot.send_message(bot_id, text='\U0001F7E1 server warning')
        config.add_section("flag")
        config.set("flag", "flag", "1")
        with open("/tmp/flag_server_warning.ini", "w") as config_file:
            config.write(config_file)
    else:
        config.read("/tmp/flag_server_warning.ini")
        flag = int(config.get("flag", "flag"))
        flag = str(flag+1)
        config.set("flag", "flag", flag)
        with open("/tmp/flag_server_warning.ini", "w") as config_file:
            config.write(config_file)
        if int(flag) > 4:
            bot.send_message(bot_id, text='\U0001F534 server unreachable')
