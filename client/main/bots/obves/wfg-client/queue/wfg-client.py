#сделал клиента, сейчас очередь запросов

#деконструктор очереди
#клиент читает поочерёдно, класс Queue над json











#частный случай очереди - экономика

#общий случай - акки, base.json = [{"id": 138,"login": "МИТРИД","planet": 12236850},]
import json
from websocket import create_connection

class Json_object(object):
    def __init__(self,path):
        with open(path) as file:self.dict = json.load(file)
    def output_dict(self):return self.dict

class Answer(Json_object):
    def __init__(self,dict):self.dict = dict
    def id(self):return self.dict["id"]
    def result(self):return self.dict["result"]

class Buildings_get_Result(Answer):
    def __init__(self,dict):self.dict = dict
    def result(self):
        self_dict = self.dict
        return self_dict

class Resources_get_Result(Answer):
    def __init__(self,dict):self.dict = dict
    def result(self):
        antimatter = self.dict["antimatter"]
        antimatter_speed = self.dict["antimatter_speed"]
        energy = self.dict["energy"]
        silicon = self.dict["silicon"]
        silicon_speed = self.dict["silicon_speed"]
        titanium = self.dict["titanium"]
        titanium_speed = self.dict["titanium_speed"]
        return antimatter,antimatter_speed,energy,silicon,silicon_speed,titanium,titanium_speed

class Planets_get_Result(Answer):
    def __init__(self,dict):self.dict = dict
    def result(self):
        base_sectors = []
        debris_silicon = []
        debris_titanium = []
        id = []
        max_temperature = []
        min_temperature = []
        name = []
        planet_energy = []
        planet_total_sectors = []
        planet_used_sectors = []
        silicon = []
        star_id = []
        titanium = []
        for n in range(len(self.dict)):
            base_sectors.append(self.dict[n]["base_sectors"])
            debris_silicon.append(self.dict[n]["debris_silicon"])
            debris_titanium.append(self.dict[n]["debris_titanium"])
            id.append(self.dict[n]["id"])
            max_temperature.append(self.dict[n]["max_temperature"])
            min_temperature.append(self.dict[n]["min_temperature"])
            name.append(self.dict[n]["name"])
            planet_energy.append(self.dict[n]["planet_energy"])
            planet_total_sectors.append(self.dict[n]["planet_total_sectors"])
            planet_used_sectors.append(self.dict[n]["planet_used_sectors"])
            silicon.append(self.dict[n]["silicon"])
            star_id.append(self.dict[n]["star_id"])
            titanium.append(self.dict[n]["titanium"])
        return base_sectors,debris_silicon,debris_titanium,id,max_temperature,min_temperature,name,planet_energy,planet_total_sectors,planet_used_sectors,silicon,star_id,titanium
        
class Request(Json_object):
    def __init__(self,dict):
        self.dict = dict
    def input_method(self):return {"method":self.dict["method"]}
    def output_method(self):return self.dict["method"]
    def input_params(self):return self.dict["params"]
    def all(self):return {**{"id":"1","jsonrpc":"2.0"},**request.input_method(),**params.all_doch()}

class Params(Request):
    def __init__(self, dict):
        self.dict = dict
    def to_doch(self):return self.dict
    def all_doch(self):
        slovar = {}
        try:slovar = {**slovar,**{"auth": auth.all()}}
        except:None
        try:slovar = {**slovar,**{"buildings.get":buildings_get.input_planet()}}
        except:None
        try:slovar = {**slovar,**{"resources.get":resources_get.all()}}
        except:None
        try:slovar = {**slovar,**{"planets.get":planets_get.all()}}
        except:None
        return {"params":slovar[request.output_method()]}

class Auth(Params):
    def __init__(self, dict):self.dict = dict
    def input_login(self):return {"login":self.dict["login"]}
    def input_pass(self):return {"pass":self.dict["pass"]}
    def all(self):return {**auth.input_login(),**auth.input_pass()}

class Buildings_get(Params):
    def __init__(self, dict):self.dict = dict
    def input_planet(self):return {"planet":self.dict["planet"]}

class Resources_get(Params):
    def __init__(self, dict):self.dict = dict
    def all(self):return {"planet":self.dict["planet"]}

class Planets_get(Params):
    def all(self):return {} 

class WFGClient:
    def __init__(self):self.ws = create_connection("wss://srv1.warforgalaxy.com:9982")
    def __del__(self):self.ws.close()
    def auth(self):
        self.ws.send(json.dumps(request.all()))
        return self.ws.recv()
    def request(self):
        self.ws.send(json.dumps(request.all()))
        with open('answer.json'.format(self), 'w') as f:
            f.write(self.ws.recv())
        return None

if __name__ == "__main__":
    wfgclient = WFGClient()
    
    json_object = Json_object('answer.json')
    answer = Answer(json_object.output_dict())
    id = answer.id()

    buildings_get_Result = Buildings_get_Result(answer.result())
    resources_get_Result = Resources_get_Result(answer.result())
    planets_get_Result = Planets_get_Result(answer.result())

    try:antimatter,antimatter_speed,energy,silicon,silicon_speed,titanium,titanium_speed = resources_get_Result.result()
    except:None
    try:base_sectors,debris_silicon,debris_titanium,id,max_temperature,min_temperature,name,planet_energy,planet_total_sectors,planet_used_sectors,silicon,star_id,titanium = planets_get_Result.result()
    except:None
    """
    try:self_dict = buildings_get_Result.result()#узнать, что в словаре
    except:None
    """

    json_object = Json_object('auth.json')
    request = Request(json_object.output_dict())
    params = Params(request.input_params())
    auth = Auth(params.to_doch())
    wfgclient.auth()

    json_object = Json_object('request.json')
    request = Request(json_object.output_dict())
    params = Params(request.input_params())
    buildings_get = Buildings_get(params.to_doch())
    resources_get = Resources_get(params.to_doch())
    planets_get = Planets_get(None)
    wfgclient.request()
