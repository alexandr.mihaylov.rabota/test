import json,configparser,requests
from random import randint
from websocket import create_connection
config = configparser.ConfigParser()
config.read("config.ini", encoding = "utf-8")
id_user = config.get("config", "id_user")
planet = str(config.get("config", "planet"))
method = config.get("config", "method")
url = "https://galaxy.smspva.space/adminka/"
base_ships = [["colossus","destroyer","bombardier","galaction","corvette","attacker","pioneer","shuttle","collector"],[0,0,0,0,0,0,0,0,0]]
for aefg in range(0,9):
        base_ships[1][aefg] = config.get("config", base_ships[0][aefg])
if method == "plus_1kkk_all_res":
    base_resources = ["titanium","silicon","antimatter"]
    for fgrt in range(0,3):
        requests.post(url+'users/0/planets/' + planet + '/resources/' + base_resources[fgrt] + '/1000000000')
elif method == "plus_13_all_economic_buildings":
    base_builds = ["neutrino_power_station","titanium_mine","silicon_mine","collider","titanium_silo","silicon_silo","antimatter_storage"]
    for fdgt in range(0,7):
        requests.post(url+'gamedata/build/update', data = {'planet':planet, 'type':base_builds[fdgt], 'level':randint(10,15)})
elif method == "plus_ships":
    for aefg in range(0,9):
        requests.post(url+'users/' + id_user + '/planets/' + planet + '/spaceships/' + base_ships[0][aefg] + '/' + base_ships[1][aefg])
else:
    login = config.get("config", "login")
    fleet_id = int(config.get("config", "fleet_id"))
    ws = create_connection("wss://srv1.warforgalaxy.com:9980/")
    ws.send(json.dumps({"id":1,"jsonrpc":"2.0","method":"auth","params":{"login":login,"pass":"CC7D7445252C8881AE037937E7803A75"}})) 
    fleet1 = "fleet1"
    fleet2 = "fleet2"
    if method == "fleets.recall":
        fleet1 = "fleet"
    if method == "fleets.arena":
        fleet2 = "fleet"
    data = {}
    for fgtr in range(0,9):
        data[base_ships[0][fgtr]] = int(base_ships[1][fgtr])
    ws.send(json.dumps({"id":1,"jsonrpc":"2.0","method":method,"params":{
        "planet":int(planet),
        fleet1:fleet_id,
        fleet2:data,
        "length":86400000000}}))
    ws.recv()
    res = json.loads(ws.recv())
    fleet_id = str(res["result"][0]["id"])
    config.set("config", "fleet_id", fleet_id)
    with open("config.ini", "w", encoding = "utf-8") as config_file:
        config.write(config_file)


"""
res = json.loads(data) 
dict = {}
keys = list(res.keys())
values = list(res.values())
for n in range(len(keys)):
    dict[keys[n]] = values[n]

x = {"id":1,"jsonrpc":"2.0","method":"auth"}
z = {**x, **dict}
qwefwef = json.dumps(z)
ws.send(qwefwef)
"""
