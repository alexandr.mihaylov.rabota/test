#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fileencoding=utf-8
import configparser
import threading
from websocket import create_connection
import datetime
config = configparser.ConfigParser()
config.read("config.ini")
host = config.get("config", "host")
amount = config.get("config", "amount")
ws = create_connection(host)
ws.send("{\"jsonrpc\":\"2.0\",\"method\":\"auth\",\"params\":{\"login\":\"recovni96\",\"pass\":\"CC7D7445252C8881AE037937E7803A75\"},\"id\":18}")
start_time_func = datetime.datetime.now()
start_time_crypt = start_time_func.hour*3600 + start_time_func.minute*60 + start_time_func.second   
def thread1():
    current_time_func = datetime.datetime.now()
    current_time_crypt = current_time_func.hour*3600 + current_time_func.minute*60 + current_time_func.second
    spent_time_crypt = current_time_crypt - start_time_crypt
    per = spent_time_crypt/(int(amount))
    f = open('stats.txt', 'w')
    f.write(str(per) + '\n')
def multithreading_function(number_of_thread):
    ws.send("{\"id\":\"1\",\"jsonrpc\":\"2.0\",\"method\":\"buildings.get\",\"params\":{\"give\":\"all\",\"planet\":9165}}")
    ws.send("{\"id\":\"2\",\"jsonrpc\":\"2.0\",\"method\":\"technologies.get\",\"params\":{}}")
    ws.send("{\"id\":\"3\",\"jsonrpc\":\"2.0\",\"method\":\"planets.resources.full_report\",\"params\":{\"planet\":9165}}")
    ws.send("{\"id\":\"4\",\"jsonrpc\":\"2.0\",\"method\":\"defence.get\",\"params\":{\"planet\":9165}}")
    ws.send("{\"id\":\"5\",\"jsonrpc\":\"2.0\",\"method\":\"defence.build.info\",\"params\":{\"planet\":9165}}")
    ws.send("{\"id\":\"6\",\"jsonrpc\":\"2.0\",\"method\":\"fleets.get\",\"params\":{}}")
    ws.send("{\"id\":\"7\",\"jsonrpc\":\"2.0\",\"method\":\"resources.get\",\"params\":{\"planet\":9165}}")
    ws.send("{\"id\":\"8\",\"jsonrpc\":\"2.0\",\"method\":\"trading.GetLots\",\"params\":{\"planet\":9165,\"rangeFrom\":1,\"rangeTo\":10000000}}")
    ws.send("{\"id\":\"9\",\"jsonrpc\":\"2.0\",\"method\":\"trading.GetLotsByUser\",\"params\":{\"planet\":9165}}")
    ws.send("{\"id\":\"10\",\"jsonrpc\":\"2.0\",\"method\":\"planets.resources.full_report\",\"params\":{\"planet\":9165}}")
    ws.send("{\"id\":\"11\",\"jsonrpc\":\"2.0\",\"method\":\"user.get\",\"params\":{}}")
    ws.send("{\"id\":\"12\",\"jsonrpc\":\"2.0\",\"method\":\"spaceships.get\",\"params\":{\"planet\":9165}}")
    ws.send("{\"id\":\"13\",\"jsonrpc\":\"2.0\",\"method\":\"trading.getlots\",\"params\":null,\"result\":{\"items\":[]}}")
    ws.send("{\"id\":\"14\",\"jsonrpc\":\"2.0\",\"method\":\"fleets.allied.attacks.list\",\"params\":{}}")
    ws.send("{\"id\":\"15\",\"jsonrpc\":\"2.0\",\"method\":\"trading.getlotsbyuser\",\"params\":null,\"result\":{\"items\":[]}}")
    ws.send("{\"id\":\"16\",\"jsonrpc\":\"2.0\",\"method\":\"fleets.enemy.attacks.list\",\"params\":{}}")
    ws.send("{\"id\":\"17\",\"jsonrpc\":\"2.0\",\"method\":\"user.get\",\"params\":{}}")
    thread1()
if __name__ == '__main__':
    for i in range(int(amount)):
        my_thread = threading.Thread(target=multithreading_function, args=(i,))
        my_thread.start()
