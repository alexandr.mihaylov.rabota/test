#!/usr/bin/env python
import json, codecs, requests, configparser,WFGClient_plus_base_and_bots_unstable,ssl,time
from http.server import BaseHTTPRequestHandler, HTTPServer
from sys import argv
class MyServer(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Credentials', 'false')
        self.send_header('Access-Control-Allow-Methods', 'POST,GET')
        self.send_header('Access-Control-Allow-Headers', 'Content-Type,content-length')
        self.end_headers()
    def do_OPTIONS(self):
        self._set_headers()
        self.wfile.write("<html><body><h1>Hello!</h1></body></html>")
    def do_GET(self):
        self._set_headers()
        self.wfile.write(bytes(json.dumps(json.load(codecs.open("base.json", "r", "utf_8_sig")), ensure_ascii=False), "Windows-1251"))#ответ
    def do_POST(self):
        self._set_headers()
        content_len = int(self.headers.get('content-length', 0))
        post_body = json.loads((self.rfile.read(content_len)).decode()) #распарcенный json, который присылает клиент
        if post_body['name'] == "go":
            d = (json.load(codecs.open("base.json", "r", "utf_8_sig")))["base"]     #запуск ботов
            for n in range(1,len(d)):
                login2,planet1,url2 = d[n][1],d[n][2],"2" #здесь начинаются боты  #0 - prod, 2 - dev 
                wFGClient4 = WFGClient_plus_base_and_bots_unstable.WFGClient(url = "wss://srv1.warforgalaxy.com:998"+url2, login = login2, password = "CC7D7445252C8881AE037937E7803A75")
                def razvedka(planet_razvedka,buildings_razvedka):
                    response = json.loads(wFGClient4.method(method = "buildings.get", give = "all", planet = planet_razvedka)) 
                    try:
                        for n in range(len(response["result"])):
                            for m in range(5):
                                if response["result"][n]["type_name"] == buildings_razvedka[0][m]:buildings_razvedka[1][m] = response["result"][n]["level"]
                    except:None
                    #for c in range(5):print(buildings_razvedka[0][c], buildings_razvedka[1][c])
                def silosfunc(silos1, buildings1):               
                    if json.loads(wFGClient4.method(method = "resources.get", planet = planet1))["result"]["titanium"] == silos1["ti"][str(buildings1[1][3])]:#ресы == склад: построить склад
                        wFGClient4.method(method = "buildings.build", planet = planet1, building_type = buildings1[0][3]) #склад ti
                        wFGClient4.method(method = "buildings.upgrade", planet = planet1, building_type = buildings1[0][0])
                buildings = [["neutrino_power_station","titanium_mine","silicon_mine","titanium_silo","collider"],[0,0,0,0,0]]#name,current_level
                ti,si,am = {"0":1005000},{"0":1005000},{"0":1000000}#вместимость склада до 30 лвл
                for n in range(1,31):ti[str(n)],si[str(n)],am[str(n)] = int((1000000*(1.5**n))+1005000),int((1000000*(1.5**n))+1005000),int((1000000*(1.5**n))+1000000)
                silos = {"ti":ti,"si":si,"am":am}
                rozvidka(planet1,buildings) 
                #print(json.loads(wFGClient4.method(method = "resources.get", planet = planet1))["result"]["energy"])            #энергия
                if json.loads(wFGClient4.method(method = "resources.get", planet = planet1))["result"]["energy"] <= 1.0: 
                    if buildings[1][0] == 0:wFGClient4.method(method = "buildings.build", planet = planet1, building_type = buildings[0][0])
                    else:wFGClient4.method(method = "buildings.upgrade", planet = planet1, building_type = buildings[0][0])
                
                def grade(vichitaemoe2,vichitaemoe3,vichitaemoe4,buildings,xerts,planet1,postfix,crem,colid): #postfix = "build"/"upgrade", crem = True/False, colid = True/False               #шахты 
                    if buildings[1][1] == (3*xerts-vichitaemoe2):wFGClient4.method(method = "buildings."+postfix, planet = planet1, building_type = buildings[0][1])
                    else:
                        if buildings[1][2] == (2*xerts-vichitaemoe3) and crem == True:wFGClient4.method(method = "buildings."+postfix, planet = planet1, building_type = buildings[0][2])
                        else:
                            if buildings[1][4] == (1*xerts-vichitaemoe4) and colid == True:wFGClient4.method(method = "buildings."+postfix, planet = planet1, building_type = buildings[0][4])
                for x in range(3):grade(3,2,1,buildings,1,planet1,"build", True,True)        
                for x in range(2):grade(2,1,0,buildings,1,planet1,"upgrade", True,False)
                for x in range(1):grade(1,0,0,buildings,1,planet1,"upgrade", False,False) 
                for xerts in range(2,9):
                    for x in range(3):grade(3,2,1,buildings,xerts,planet1,"upgrade", True,True)
                    for x in range(2):grade(2,1,0,buildings,xerts,planet1,"upgrade", True,False)
                    for x in range(1):grade(1,0,0,buildings,xerts,planet1,"upgrade", False,False)

        else: #здесь регистрация нового бота
            d = (json.load(codecs.open("base.json", "r", "utf_8_sig")))["base"]
            requests.post("https://api.warforgalaxy.com/user/register-private", data = {"name": post_body["name"],"password":"AAEDDAA434BE0BDB5424875FB826984F"})#paroldlyabotov
            config = configparser.ConfigParser()
            config.add_section("config")
            config.set("config","name",post_body["name"])       
            config.set("config","planet","0")
            config.set("config","id","0")
            with open("config.ini", "w", encoding = 'utf-8') as config_file:config.write(config_file)
            config = configparser.ConfigParser()#начало добычи id планеты и id user
            config.read("config.ini", encoding = "utf-8")
            wFGClient = WFGClient_plus_base_and_bots_unstable.WFGClient(url = "wss://srv1.warforgalaxy.com:9982", login = config.get("config","name"), password = "CC7D7445252C8881AE037937E7803A75")
            planet = str(json.loads(wFGClient.method(method = "planets.get"))["result"][0]["id"])
            id = str(json.loads(wFGClient.method(method = "user.get"))["result"]["id"])
            config.set("config","planet",planet)
            config.set("config","id",id)
            with open("config.ini", "w", encoding = 'utf-8') as config_file:config.write(config_file)#конец добычи id планеты и id user
            config.read("config.ini", encoding = "utf-8")
            planet = int(config.get("config", "planet"))
            id = int(config.get("config", "id"))
            dop = [id,post_body["name"],planet]
            d.append(dop)
            dict1 = {"base":d}
            f = codecs.open("base.json", "w", "utf_8_sig")
            f.write(json.dumps(dict1))
        self.wfile.write(bytes(json.dumps(json.loads('{"result":"done"}'), ensure_ascii=False), "Windows-1251"))#ответ
if __name__ == "__main__":
    myServer = HTTPServer(("192.168.0.127", 9000), MyServer)
    """myServer.socket = ssl.wrap_socket (myServer.socket, #try
        keyfile='/etc/letsencrypt/live/srv1.warforgalaxy.com/privkey.pem',
        certfile='/etc/letsencrypt/live/srv1.warforgalaxy.com/fullchain.pem', server_side=True)"""
    myServer.serve_forever()
