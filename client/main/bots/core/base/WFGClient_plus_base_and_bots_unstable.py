import json, uuid, requests, time
from websocket import create_connection 
class WFGClient:
    def __init__(self, **kwargs):
        self.ws,self.json,self.kwargs,self.params = create_connection(kwargs["url"]),{"jsonrpc":"2.0"},["id","method","params"],{}
        self.ws.send(json.dumps({**self.json,**{self.kwargs[0]:str(uuid.uuid4())},**{self.kwargs[1]:"auth"},**{self.kwargs[2]:{"login":kwargs["login"],"pass":kwargs["password"]}}}))
        self.ws.recv()#debug
    def method(self,**kwargs):
        params,flag = [],0
        planet = ["resources.get","buildings.get","buildings.build","buildings.upgrade","technologies.develop","spaceships.build","defence.build","spaceships.build.info","spaceships.cancel"] # 1 str = 1 param + n-methods
        for cn in range(len(planet)):
            if kwargs[self.kwargs[1]] == planet[cn]:params.append("planet")
        building_type = ["buildings.build","buildings.upgrade","spaceships.build","defence.build"]
        for cn in range(len(building_type)):
            if kwargs[self.kwargs[1]] == building_type[cn]:params.append("building_type")
        technology_type = ["technologies.develop"]
        for cn in range(len(technology_type)):
            if kwargs[self.kwargs[1]] == technology_type[cn]:params.append("technology_type")
        give = ["buildings.get"]
        for cn in range(len(give)):
            if kwargs[self.kwargs[1]] == give[cn]:params.append("give")
        amount = ["spaceships.build","defence.build"]
        for cn in range(len(amount)):
            if kwargs[self.kwargs[1]] == amount[cn]:params.append("amount")
        if kwargs[self.kwargs[1]] == "fleets.send":     # 1 str = 1 method + n-params 
            params = ["planet_from","planet_to","path_from","path_to","both_way","task","fill_up","both_ways","visible","flight_speed_percent"]
            cargo,cargo_keys = {},["titanium","silicon","antimatter"]
            for n in range(3):
                flag = flag + 1
                try:cargo = {**cargo,**{cargo_keys[n]:kwargs[cargo_keys[n]]}}
                except:flag = flag - 1
            self.params = {**self.params,**{"cargo":cargo}}
            fleet,fleet_keys = {},["shuttle","transport","fighter","attacker","corvette","frigate","galaction","destroyer","bombardier","colossus","scout","energodrone","collector","pioneer"]
            for n in range(14):
                flag = flag + 1
                try:fleet = {**fleet,**{fleet_keys[n]:kwargs[fleet_keys[n]]}}
                except:flag = flag - 1
            self.params = {**self.params,**{"fleet":fleet}}
            """fleet,fleet_keys = [],["shuttle","transport","fighter","attacker","corvette","frigate","galaction","destroyer","bombardier","colossus","scout","energodrone","collector","pioneer"]#for massives
            for n in range(14):
                flag = flag + 1
                try:fleet.append(kwargs[fleet_keys[n]])
                except:flag = flag - 1
            self.params = {**self.params,**{"fleet":fleet}}"""
        for n in range(len(kwargs)-1-flag):self.params = {**self.params,**{params[n]:kwargs[params[n]]}} 
        self.ws.send(json.dumps({**self.json,**{self.kwargs[0]:str(uuid.uuid4())},**{self.kwargs[1]:kwargs[self.kwargs[1]]},**{self.kwargs[2]:self.params}})) 
        #print(json.dumps({**self.json,**{self.kwargs[0]:str(uuid.uuid4())},**{self.kwargs[1]:kwargs[self.kwargs[1]]},**{self.kwargs[2]:self.params}}))
        return self.ws.recv()#debug
    def __del__(self):self.ws.close()
